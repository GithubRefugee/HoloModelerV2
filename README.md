# BLNK'S HOLOMODELER V2

An easy to use tool to create holographic models in Garry's Mod.

### Install
1. Open holomodeler_v2.txt file
2. Press Open Raw (2nd button in bar above code)
3. Press CTRL+S or right click and Save As
4. Place in expression2 folder (garrysmod/data/expression2)
5. You may also download the documentation, it is also an E2 you can place

## Usage
* I have already used HoloModeler V1
    1. Read guide for V1 users in documentation (1.2)
    2. Read about new features and how to use them (1.1)

* I am a new user
    1. Read guide for new users in documentation (1.3)
    2. Read basic usage chapter (3)

* Video tutorial??
    * Maybe in the future